FROM fedora:25
MAINTAINER Serge Skripchuk <tarzanych@gmail.com>

RUN yum install -y unzip make wget curl git gawk libzip-devel \
    freetds-devel.x86_64 freetds.x86_64 \
    openssl-devel openssl sbcl clisp

ADD ./src /opt/src/pgloader
WORKDIR /opt/src/pgloader

RUN cd /opt/src/pgloader \
    && mkdir -p build/bin \
    && make \
    && cp /opt/src/pgloader/build/bin/pgloader /usr/local/bin
